<?php

namespace App\Http\Controllers;

use App\ProviderDocument;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ProviderApiController extends Controller
{
    public function updateDocument(Request $request, $pid, $docid) {
        try {
            $document = ProviderDocument::where('provider_id', $pid)
                ->where('document_id', $docid)
                ->firstOrFail();

            $document->update([
                'url' => $request->document->store('provider/documents'),
                'status' => 'ASSESSING',
            ]);

            return response()->json([
                'stat' => 'success',
                'code' => 0,
                'message' => 'Updated successfully',
            ]);
        } catch (ModelNotFoundException $e) {
            ProviderDocument::create([
                'url' => $request->document->store('provider/documents'),
                'provider_id' => $pid,
                'document_id' => $docid,
                'status' => 'ASSESSING',
            ]);

            return response()->json([
                'stat' => 'success',
                'code' => 0,
                'message' => 'Created successfully',
            ]);
        }
    }
}
